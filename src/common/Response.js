module.exports = class Response {
    constructor(status, message) {
        this.status = status;
        this.message = message;
    }
}