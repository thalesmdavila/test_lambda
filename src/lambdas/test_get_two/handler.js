'use strict';

const Response = require('../../common/Response');
 
console.log('Loading function');

module.exports.test_get_two = async (event, context) => {
    console.log(event);
    let status = process.env.DB_NAME === 'test'
    
    const response = new Response(status, "works")
    return {"statusCode": 200, "body": JSON.stringify(response)};
};