'use strict';

const Response = require('../../common/Response');

console.log('Loading function');

module.exports.test_post = async (event, context) => {
    console.log(process.env.DB_NAME)
    const response = new Response(event.body, "works")
    return {"statusCode": 200, "body": JSON.stringify(response)};
};
