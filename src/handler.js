'use strict';
 
const msg = require('./msg.js')

module.exports.hello = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Your function executed successfully! ! !',
        status: msg.status
      },
      null,
      2
    ),
  };
};